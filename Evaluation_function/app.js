//define the regular expression
let regular;
regular = /hello/;
//regular = /hello/i;   //i means, its case insensitive;
//regular = /hello/g;   //g means, global search, its search entire all things, not a single paragraph or first match;

//define the string
const string = "hello world";

//exec()   -> if match then provide an array,matching index and otherwise null
/*const result = regular.exec(string);

console.log(result);
console.log(result.index);
console.log(result.input);
console.log(result[0]);*/


//test() ->if match then true, otherwise false;
/*const result = regular.test('hello');
console.log(result);*/


//match() ->its work with
